import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashPage extends StatelessWidget {
  final Color backgroundColor;

  const SplashPage({
    Key? key,
    this.backgroundColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width / 2;

    return Material(
      child: Container(
        color: backgroundColor,
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/images/background.svg',
                  width: width,
                ),
                SizedBox(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Text('tada',
                      style: Theme.of(context).textTheme.headline1!.apply(
                        color: Theme.of(context).primaryColor,
                        fontFamily: 'Poppins'
                      ),
                    )
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.0),
                  child: Text('A simple todo app',
                    style: Theme.of(context).textTheme.headline5!.apply(
                      color: Theme.of(context).hintColor,
                        fontFamily: 'Poppins'
                    ),
                  )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
