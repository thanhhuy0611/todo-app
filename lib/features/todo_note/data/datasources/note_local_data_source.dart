import 'dart:io';

import 'package:objectbox/objectbox.dart';
import 'package:todo_app/features/todo_note/data/models/note.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/objectbox.g.dart';

class NoteLocalDataSoure {
  late Store _store;
  late Box<NoteModel> _box;
  
  NoteLocalDataSoure._internal();

  static final NoteLocalDataSoure _singleton = NoteLocalDataSoure._internal();

  static NoteLocalDataSoure get instance {
    return _singleton;
  }

  void initialize(Directory dir) {
    _store = Store(getObjectBoxModel(), directory: dir.path + '/objectbox');
    _box = _store.box<NoteModel>();
  }

  List<Note> getNote({bool? isDone}) {
    List<NoteModel> listModel = [];
    if (isDone == null) {
      listModel = _box.getAll();  
    } else {
      final query = _box.query(NoteModel_.isDone.equals(isDone)).build();
      listModel = query.find();
    }

    return listModel.map((model) => model.toEntity())
        .toList();
  }

  Note upsertNote(Note note) {
    // convert Entity to Model
    final noteModel = NoteModel.fromEntity(note);
    // upsert to DB
    int id = _box.put(NoteModel.fromEntity(note));
    // update noteId
    noteModel.id = id;
    return noteModel.toEntity();
  }

  bool deleteNote(int noteId) {
    return _box.remove(noteId);
  }

}