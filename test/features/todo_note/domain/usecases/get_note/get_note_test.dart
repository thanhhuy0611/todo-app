import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';
import 'package:todo_app/features/todo_note/domain/usecases/get_all_note.dart';

class MockNoteRepository extends Mock implements NoteRepository {
  @override
  List<Note> getNote({bool? isDone}) => super.noSuchMethod(
    Invocation.method(#getNote, [isDone]),
    returnValue: [Note(isDone: true, title: 'example')].toList()
  );
}

void main() {
  late GetNote usecase;
  late MockNoteRepository mockNoteRepository;

  setUp(() {
    mockNoteRepository = MockNoteRepository();
    usecase = GetNote(mockNoteRepository);
  });

  final doneNote1 = Note(title: "Done Note 1", isDone: true);
  final doneNote2 = Note(title: "Done Note 2", isDone: true);
  final todoNote1 = Note(title: "Todo Note 1", isDone: false);

  test(
    'should get all notes from the repository', 
    () async {
      // Use Mockito to return a result of method getNote()
      when(mockNoteRepository.getNote())
        .thenAnswer((_) => [doneNote1, doneNote2, todoNote1]);

      // Call method get all note
      final result = usecase();

      // UseCase should simply return all done and todo notes
      expect(result, [doneNote1, doneNote2, todoNote1]);

      // Verify that the method has been called on the Repository
      verify(mockNoteRepository.getNote());

      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockNoteRepository);
    },
  );

  test(
    'should get only done notes from the repository', 
    () async {
      // Use Mockito to return a result of method getNote()
      when(mockNoteRepository.getNote(isDone: true))
        .thenAnswer((_) => [doneNote1, doneNote2]);

      // Call method get all note
      final result = usecase(isDone: true);

      // UseCase should simply return all done and todo notes
      expect(result, [doneNote1, doneNote2]);

      // Verify that the method has been called on the Repository
      verify(mockNoteRepository.getNote(isDone: true));

      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockNoteRepository);
    },
  );

  test(
    'should get only todo notes from the repository', 
    () async {
      // Use Mockito to return a result of method getNote()
      when(mockNoteRepository.getNote(isDone: false))
        .thenAnswer((_) => [todoNote1]);

      // Call method get all note
      final result = usecase(isDone: false);

      // UseCase should simply return all done and todo notes
      expect(result, [todoNote1]);

      // Verify that the method has been called on the Repository
      verify(mockNoteRepository.getNote(isDone: false));

      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockNoteRepository);
    },
  );
}