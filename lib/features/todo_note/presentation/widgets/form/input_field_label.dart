import 'package:flutter/material.dart';

class InputFieldLabel extends StatelessWidget {
  final String label;

  const InputFieldLabel(this.label, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 15.0,
        bottom: 5.0,
      ),
      child: Text(
        label,
        style: TextStyle(
          color: Theme.of(context).hintColor,
          fontWeight: FontWeight.bold,
          fontSize: 14.0,
        ),
      ),
    );
  }
}
