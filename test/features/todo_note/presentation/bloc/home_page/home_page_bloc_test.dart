import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/home_page/home_page_bloc.dart';

import '../dependency_class.mocks.dart';

void main() {
  final initialState = HomePageState();
  final initializedState = HomePageState(
    loaded: true, 
    allNotes: [], 
    doneNotes: [], 
    todoNotes: [],
  );
  final todoNote = Note(title: "Text", isDone: false);
  final doneNote = Note(title: "Text", isDone: true);
  
  late HomePageBloc bloc;
  late MockNoteRepositoryImplement mockNoteRepository;

  setUp(() {
    mockNoteRepository = MockNoteRepositoryImplement();
    bloc = HomePageBloc(mockNoteRepository);
  });

  tearDown(() {
    bloc.close();
  });

  group('HomePageBloc', () {
    test('initial state is correct', () {
      expect(bloc.state, initialState);
    });

    group('HomePageInit', () {
      blocTest<HomePageBloc, HomePageState>(
        'should emit initializedState which contain [allNotes,doneNotes,todoNotes] data after init',
        build: () {
          when(mockNoteRepository.getNote(isDone: anyNamed('isDone'))).thenAnswer((_) => <Note>[]);
          return bloc;
        },
        act: (bloc) => bloc.add(HomePageInit()),
        expect: () => [initializedState],
        verify: (_) {
          verify(mockNoteRepository.getNote(isDone: anyNamed('isDone'))).called(3);
        },
        tearDown: () => bloc.close(),
      );
    });

    group('HomePageToggleNote', () {
      blocTest<HomePageBloc, HomePageState>('should add HomePageInit to Bloc after upsert Note', 
        build: () {
          when(mockNoteRepository.upsertNote(any)).thenAnswer((_) => doneNote);
          when(mockNoteRepository.getNote(isDone: anyNamed('isDone'))).thenAnswer((_) => <Note>[]);
          return bloc;
        },
        act: (bloc) => bloc.add(HomePageToggleNote(note: todoNote, status: true)),
        verify: (_) {
          verify(mockNoteRepository.upsertNote(any)).called(1);
          // check whether HomePageState was re-init or not
          verify(mockNoteRepository.getNote(isDone: anyNamed('isDone'))).called(3);
        },
        tearDown: () => bloc.close(),
      );
    });
  });
}
