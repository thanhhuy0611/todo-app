import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';

class DeleteNote {
  final NoteRepository noteRepository;

  DeleteNote(this.noteRepository);

  bool call(int noteId) {
    return noteRepository.deleteNote(noteId);
  }
}