// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: camel_case_types

import 'dart:typed_data';

import 'package:objectbox/flatbuffers/flat_buffers.dart' as fb;
import 'package:objectbox/internal.dart'; // generated code can access "internal" functionality
import 'package:objectbox/objectbox.dart';
import 'package:objectbox_flutter_libs/objectbox_flutter_libs.dart';

import 'features/todo_note/data/models/note.dart';

export 'package:objectbox/objectbox.dart'; // so that callers only have to import this file

final _entities = <ModelEntity>[
  ModelEntity(
      id: const IdUid(2, 9136020740828638551),
      name: 'NoteModel',
      lastPropertyId: const IdUid(5, 4086700513229618067),
      flags: 0,
      properties: <ModelProperty>[
        ModelProperty(
            id: const IdUid(1, 5321523761442159122),
            name: 'id',
            type: 6,
            flags: 1),
        ModelProperty(
            id: const IdUid(2, 8411362571638870012),
            name: 'isDone',
            type: 1,
            flags: 0),
        ModelProperty(
            id: const IdUid(3, 2592143032816947541),
            name: 'title',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(4, 5705616061636839958),
            name: 'description',
            type: 9,
            flags: 0),
        ModelProperty(
            id: const IdUid(5, 4086700513229618067),
            name: 'date',
            type: 10,
            flags: 0)
      ],
      relations: <ModelRelation>[],
      backlinks: <ModelBacklink>[])
];

/// Open an ObjectBox store with the model declared in this file.
Future<Store> openStore(
        {String? directory,
        int? maxDBSizeInKB,
        int? fileMode,
        int? maxReaders,
        bool queriesCaseSensitiveDefault = true,
        String? macosApplicationGroup}) async =>
    Store(getObjectBoxModel(),
        directory: directory ?? (await defaultStoreDirectory()).path,
        maxDBSizeInKB: maxDBSizeInKB,
        fileMode: fileMode,
        maxReaders: maxReaders,
        queriesCaseSensitiveDefault: queriesCaseSensitiveDefault,
        macosApplicationGroup: macosApplicationGroup);

/// ObjectBox model definition, pass it to [Store] - Store(getObjectBoxModel())
ModelDefinition getObjectBoxModel() {
  final model = ModelInfo(
      entities: _entities,
      lastEntityId: const IdUid(2, 9136020740828638551),
      lastIndexId: const IdUid(0, 0),
      lastRelationId: const IdUid(0, 0),
      lastSequenceId: const IdUid(0, 0),
      retiredEntityUids: const [842535630411446689],
      retiredIndexUids: const [],
      retiredPropertyUids: const [
        6175659487490182377,
        2635420750162527485,
        2119703757326027952,
        7307355233940471585,
        554323436695408802
      ],
      retiredRelationUids: const [],
      modelVersion: 5,
      modelVersionParserMinimum: 5,
      version: 1);

  final bindings = <Type, EntityDefinition>{
    NoteModel: EntityDefinition<NoteModel>(
        model: _entities[0],
        toOneRelations: (NoteModel object) => [],
        toManyRelations: (NoteModel object) => {},
        getId: (NoteModel object) => object.id,
        setId: (NoteModel object, int id) {
          object.id = id;
        },
        objectToFB: (NoteModel object, fb.Builder fbb) {
          final titleOffset = fbb.writeString(object.title);
          final descriptionOffset = object.description == null
              ? null
              : fbb.writeString(object.description!);
          fbb.startTable(6);
          fbb.addInt64(0, object.id ?? 0);
          fbb.addBool(1, object.isDone);
          fbb.addOffset(2, titleOffset);
          fbb.addOffset(3, descriptionOffset);
          fbb.addInt64(4, object.date.millisecondsSinceEpoch);
          fbb.finish(fbb.endTable());
          return object.id ?? 0;
        },
        objectFromFB: (Store store, ByteData fbData) {
          final buffer = fb.BufferContext(fbData);
          final rootOffset = buffer.derefObject(0);

          final object = NoteModel(
              id: const fb.Int64Reader()
                  .vTableGetNullable(buffer, rootOffset, 4),
              title:
                  const fb.StringReader().vTableGet(buffer, rootOffset, 8, ''),
              isDone:
                  const fb.BoolReader().vTableGet(buffer, rootOffset, 6, false),
              description: const fb.StringReader()
                  .vTableGetNullable(buffer, rootOffset, 10),
              date: DateTime.fromMillisecondsSinceEpoch(
                  const fb.Int64Reader().vTableGet(buffer, rootOffset, 12, 0)));

          return object;
        })
  };

  return ModelDefinition(model, bindings);
}

/// [NoteModel] entity fields to define ObjectBox queries.
class NoteModel_ {
  /// see [NoteModel.id]
  static final id = QueryIntegerProperty<NoteModel>(_entities[0].properties[0]);

  /// see [NoteModel.isDone]
  static final isDone =
      QueryBooleanProperty<NoteModel>(_entities[0].properties[1]);

  /// see [NoteModel.title]
  static final title =
      QueryStringProperty<NoteModel>(_entities[0].properties[2]);

  /// see [NoteModel.description]
  static final description =
      QueryStringProperty<NoteModel>(_entities[0].properties[3]);

  /// see [NoteModel.date]
  static final date =
      QueryIntegerProperty<NoteModel>(_entities[0].properties[4]);
}
