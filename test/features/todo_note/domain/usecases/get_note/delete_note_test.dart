import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';
import 'package:todo_app/features/todo_note/domain/usecases/delete_note.dart';

class MockNoteRepository extends Mock implements NoteRepository {
  @override
  bool deleteNote(int noteId) => super.noSuchMethod(
    Invocation.method(#deleteNote, [noteId]),
    returnValue: true,
  );
}

void main() {
  late MockNoteRepository mockNoteRepository;
  late DeleteNote usecase;

  setUp((){
    mockNoteRepository = MockNoteRepository();
    usecase = DeleteNote(mockNoteRepository);
  });

  final existNoteId = 1;
  final notExistNoteId = 999;
  group(
    'delete note',
    () {
      test(
        'should return true if delete success',
        () async {
          when(mockNoteRepository.deleteNote(existNoteId))
            .thenAnswer((_) => true);
          final result = usecase(existNoteId);
          expect(result, true);
        }
      );

      test(
        'should return false if delete failure',
        () async {
          when(mockNoteRepository.deleteNote(notExistNoteId))
            .thenAnswer((_) => false);
          final result = usecase(notExistNoteId);
          expect(result, false);
        }
      );
    }
  );
}