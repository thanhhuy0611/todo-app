import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:todo_app/features/todo_note/data/repositories/note_repository_imp.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/usecases/get_all_note.dart';
import 'package:todo_app/features/todo_note/domain/usecases/upsert_note.dart';

part 'home_page_event.dart';
part 'home_page_state.dart';

class HomePageBloc extends Bloc<HomePageEvent, HomePageState> {
  final GetNote _getNote;
  final UpsertNote _upsertNote;

  HomePageBloc(
    NoteRepositoryImplement noteRepository,
  ) : _getNote = GetNote(noteRepository),
      _upsertNote = UpsertNote(noteRepository),
      super(HomePageState());

  @override
  Stream<HomePageState> mapEventToState(
    HomePageEvent event,
  ) async* {
    if (event is HomePageInit) {
      yield state.copyWith(
        allNotes: _getNote(),
        doneNotes: _getNote(isDone: true),
        todoNotes: _getNote(isDone: false),
        loaded: true,
      );
    }
    if (event is HomePageToggleNote) {
      final updatedNote = event.note.coppyWith(isDone: event.status);

      // insert to DB
      _upsertNote.call(updatedNote);

      //reload homepage
      add(HomePageInit());
    }
  }
}
