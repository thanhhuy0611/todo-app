import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class HomePageBottomBar extends StatelessWidget {
  final int currentIndex;
  final int totalTabs;
  final Function(int) onTap;
  
  const HomePageBottomBar({
    Key? key, 
    required this.currentIndex,
    required this.totalTabs, 
    required this.onTap}) : super(key: key);

  List<Widget> _buildTabButton(BuildContext context) {
    final widthButton = MediaQuery.of(context).size.width/totalTabs;

    final icons = [
      Icon(Icons.list),
      Icon(Icons.checklist),
      Icon(Icons.assignment_late_outlined),
    ];

    final tabButtons = List<Widget>.generate(totalTabs, (index) {
      return Container(
        padding: EdgeInsets.all(8.0),
        width: widthButton,
        child: TextButton(
          style: TextButton.styleFrom(
            primary: currentIndex == index
              ? Theme.of(context).primaryColor
              : Theme.of(context).disabledColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
          ),
          onPressed: () => onTap(index), 
          child: icons[index],
        ),
      );
    });

    return tabButtons;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.vertical(top: Radius.circular(24.0)),
        color: const Color(0xFFF9F9F9), 
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            offset: const Offset(0.0, -0.5),
          ),
        ]),
      child: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildTabButton(context),
        )
      ),
    );
  }
}