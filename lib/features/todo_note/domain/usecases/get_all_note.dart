import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';

class GetNote {
  final NoteRepository noteRepository;

  GetNote(this.noteRepository);

  List<Note> call({bool? isDone}) {
    return noteRepository.getNote(isDone: isDone);
  }
}