import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/home_page/home_page_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/index.dart';
import 'package:todo_app/route/route_arguments.dart';

class NoteItemList extends StatelessWidget {
  final Note note;
  const NoteItemList({ Key? key, required this.note }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        var shouldReload = await Navigator.of(context).pushNamed(
          NoteForm.pageName,
          arguments: NoteFormArguments(note: note),
        );
        if (shouldReload as bool? ?? false) {
          BlocProvider.of<HomePageBloc>(context).add(HomePageInit());
        }
      },
      child: Container(
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFE6E6E6),
              offset: Offset(0, 4),
              blurRadius: 4,
              spreadRadius: 1,
            )
          ],
        ),
        margin: EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        padding: EdgeInsets.all(8),
        child: Row(
          children: [
            Checkbox(
              value: note.isDone, 
              onChanged: (bool? value) {
                BlocProvider.of<HomePageBloc>(context).add(HomePageToggleNote(
                  status: value!,
                  note: note,
                ));
              },
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(note.title,
                    style: Theme.of(context).textTheme.headline5,
                  ),
                  SizedBox(height: 8,),
                  note.description == null || note.description!.isEmpty
                    ? SizedBox()
                    : Text(note.description!),  
                ]
              ),
            ),
          ],
        ),
      ),
    );
  }
}