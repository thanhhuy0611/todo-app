import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';
import 'package:todo_app/features/todo_note/data/repositories/note_repository_imp.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/usecases/delete_note.dart';
import 'package:todo_app/features/todo_note/domain/usecases/upsert_note.dart';
import 'package:todo_app/helper/validator/note/description.dart';
import 'package:todo_app/helper/validator/note/title.dart';

part 'form_note_event.dart';
part 'form_note_state.dart';

class FormNoteBloc extends Bloc<FormNoteEvent, FormNoteState> {
  final UpsertNote _upsertNote;
  final DeleteNote _deleteNote;

  FormNoteBloc({
    required NoteRepositoryImplement noteRepository,
    Note? note,
  }) : _upsertNote = UpsertNote(noteRepository),
      _deleteNote = DeleteNote(noteRepository),
      super(FormNoteState.init(note));

  @override
  Stream<FormNoteState> mapEventToState(
    FormNoteEvent event,
  ) async* {
    if (event is FormNoteDelete) {
      if (state.id != null) {
        _deleteNote.call(state.id!);

        yield state.copyWith(status: FormzStatus.submissionSuccess);
      }
    }
    if (event is FormNoteTitleChange) {
      final title = NoteTitleField.dirty(event.title);
      yield state.copyWith(title: title);
    }
    if (event is FormNoteDescriptionChange) {
      final description = NoteDescriptionField.dirty(event.description);
      yield state.copyWith(description: description);
    }
    if (event is FormNoteToggleStatus) {
      yield state.copyWith(isDone: event.isDone);
    }
    if (event is FormNoteSubmit) {
      yield state.copyWith(status: FormzStatus.submissionInProgress);

      final title = NoteTitleField.dirty(state.title.value);
      final description = NoteDescriptionField.dirty(state.description?.value);

      FormzStatus validate = Formz.validate([title, description]);
      if (validate == FormzStatus.valid) {
        // create a note instance and upsert to DB
        _upsertNote.call(Note(
          id: state.id,
          title: state.title.value!,
          description: state.description?.value,
          isDone: state.isDone,
          date: state.date,
        ));

        yield state.copyWith(status: FormzStatus.submissionSuccess);
      } else {
        yield state.copyWith(
          status: FormzStatus.invalid,
          title: title,
          description: description,
        );
      }
      yield state.copyWith(status: FormzStatus.pure);
    }
  }
}
