part of 'form_note_bloc.dart';

class FormNoteState extends Equatable {
  final int? id;
  final NoteTitleField title;
  final NoteDescriptionField? description;
  final DateTime date;
  final bool isDone;
  final FormzStatus status;

  const FormNoteState({
    this.id,
    required this.title, 
    required this.date, 
    required this.isDone,
    required this.status,
    this.description,
  });

  factory FormNoteState.init(Note? note) {
    return FormNoteState(
      id: note?.id,
      title: NoteTitleField.pure(note?.title ?? ''), 
      description: NoteDescriptionField.pure(note?.description ?? ''), 
      isDone: note?.isDone ?? false,
      date: DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, DateTime.now().hour, DateTime.now().minute, DateTime.now().second),
      status: FormzStatus.pure,
    );
  }

  FormNoteState copyWith({
    int? id,
    NoteTitleField? title,
    NoteDescriptionField? description,
    DateTime? date,
    bool? isDone,
    FormzStatus? status,
  }) {
    return FormNoteState(
      id: id ?? this.id,
      title: title ?? this.title, 
      description: description ?? this.description, 
      date: date ?? this.date, 
      isDone: isDone ?? this.isDone,
      status: status ?? this.status,
    );
  }

  @override
  List<Object?> get props => [
    id,
    title, 
    description, 
    date, 
    isDone,
    status,
  ];
}

