import 'package:flutter/material.dart';

class CommonTheme {
  final Color mainColor = const Color(0xff8989D0);
  final Color primaryColor = const Color(0xFF9774E1);
  final Color accentColor = const Color(0xFF9F1DF0);
  final Color backgroundColor = const Color(0xFFFFFFFF);
  final Color dividerColor = const Color(0xFFF2F2F2);
  final Color hintColor = const Color(0xFF999999);
  final String fontFamily = 'Arial';

  TextStyle? headLine5 =
      const TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700, color: Color.fromRGBO(0, 0, 0, 0.87));
  TextStyle? headLine6 = const TextStyle(fontSize: 18.0, fontStyle: FontStyle.normal, color: Colors.black87);
  TextStyle? button = const TextStyle(fontSize: 14.0, color: Colors.white);
  TextStyle? subtitle2 = const TextStyle(fontSize: 16.0, color: Colors.black87);
  TextStyle? bodyText2 = const TextStyle(fontSize: 16.0, color: Color.fromRGBO(0, 0, 0, 0.87));
  TextStyle? bodyText1 = const TextStyle(fontSize: 14.0, color: Color.fromRGBO(0, 0, 0, 0.87));
  TextStyle? caption = const TextStyle(fontSize: 12.0, color: Color.fromRGBO(0, 0, 0, 0.26));

  CommonTheme();

  static Color get primary => const Color(0xFF9774E1);

  ThemeData get theme {
    return ThemeData(
      backgroundColor: backgroundColor,
      scaffoldBackgroundColor: backgroundColor,
      brightness: Brightness.light,
      primaryColor: primaryColor,
      accentColor: accentColor,
      dividerColor: dividerColor,
      hintColor: hintColor,
      fontFamily: fontFamily,
      // Define the default TextTheme. Use this to specify the default
      // text styling for headlines, titles, bodies of text, and more.
      textTheme: TextTheme(
        headline5: headLine5,
        headline6: headLine6,
        button: button,
        subtitle2: subtitle2,
        bodyText1: bodyText1,
        bodyText2: bodyText2,
        caption: caption,
      ),
      visualDensity: VisualDensity.adaptivePlatformDensity,
      textSelectionTheme: TextSelectionThemeData(cursorColor: mainColor),
    );
  }
}
