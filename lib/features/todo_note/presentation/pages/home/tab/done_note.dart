import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/home_page/home_page_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/app_loader.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/no_content_page.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/note_item_list/index.dart';
import 'package:collection/collection.dart';

class DoneNoteTab extends StatelessWidget {
  const DoneNoteTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomePageBloc, HomePageState>(
      buildWhen: (prev, curr) => DeepCollectionEquality().equals(prev.doneNotes,prev.doneNotes),
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Done"),
          ),
          body: !state.loaded || state.doneNotes == null
            ? AppLoader()
            : state.doneNotes!.isEmpty
              ? NoContentPage(text: 'Looks like no notes have been completed yet')
              : ListView.builder(
                  itemCount: state.doneNotes!.length,
                  itemBuilder: (context, index) {
                    return NoteItemList(note: state.doneNotes![index]);
                  },
                ),
        );
      },
    );
  }
}