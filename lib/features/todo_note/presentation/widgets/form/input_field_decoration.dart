import 'package:flutter/material.dart';

class InputFieldDecoration {
  InputDecoration inputDecoration({
    required String hint,
    String? errText,
    double vertical = 21.0,
    Color hintColor = const Color(0xFFBFBFBF),
    Color fillColor = const Color(0xFFF2F2F2),
    Widget? suffixIcon,
    Widget? prefixIcon,
    bool dense = false,
    double raduis = 10.0,
    bool bold = true,
  }) {
    return InputDecoration(
      isDense: dense,
      hintText: hint,
      hintStyle: TextStyle(
        color: hintColor,
        fontWeight: !!bold ? FontWeight.bold : FontWeight.normal,
      ),
      contentPadding: EdgeInsets.symmetric(
        vertical: vertical,
        horizontal: 15.0,
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide.none,
        borderRadius: BorderRadius.all(
          Radius.circular(raduis),
        ),
      ),
      filled: true,
      fillColor: fillColor,
      errorText: errText,
      errorStyle: errorStyle(),
      suffixIcon: suffixIcon,
      prefixIcon: prefixIcon,
    );
  }

  InputDecoration editInputDecoration({
    String? label,
    String? errText = '',
    FontWeight fontWeight = FontWeight.bold,
  }) {
    return InputDecoration(
      isDense: true,
      contentPadding: const EdgeInsets.symmetric(vertical: 2.0),
      hintText: label,
      hintStyle: TextStyle(
        fontWeight: fontWeight,
      ),
      border: InputBorder.none,
      errorText: errText,
      errorStyle: errorStyle(),
    );
  }

  static TextStyle errorStyle() {
    return const TextStyle(
      color: Colors.red,
      fontSize: 9.0,
      height: 0.3,
      textBaseline: TextBaseline.ideographic,
    );
  }
}
