import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/home_page/home_page_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/app_loader.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/no_content_page.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/note_item_list/index.dart';
import 'package:collection/collection.dart';

class TodoNoteTab extends StatelessWidget {
  const TodoNoteTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomePageBloc, HomePageState>(
      buildWhen: (prev, curr) => DeepCollectionEquality().equals(prev.todoNotes,prev.todoNotes),
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Todo"),
          ),
          body: !state.loaded || state.todoNotes == null
            ? AppLoader()
            : state.todoNotes!.isEmpty
              ? NoContentPage(text: 'Great! You don\'t have any notes need to do')
              : ListView.builder(
                  itemCount: state.todoNotes!.length,
                  itemBuilder: (context, index) {
                    return NoteItemList(note: state.todoNotes![index]);
                  },
                ),
        );
      },
    );
  }
}