import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/form_note/form_note_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/form/input_field_decoration.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/form/input_field_label.dart';

class NoteFormDescription extends StatelessWidget with InputFieldDecoration {
  const NoteFormDescription({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FormNoteBloc, FormNoteState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InputFieldLabel('Description'),
              TextFormField(
                decoration: inputDecoration(
                  hint: 'Description more ...',
                  errText: state.status == FormzStatus.invalid
                    ? state.title.errorText()
                    : '',
                ),
                minLines: 6,
                maxLines: 10,
                initialValue: state.description?.value,
                onChanged: (String value) {
                  BlocProvider.of<FormNoteBloc>(context).add(FormNoteDescriptionChange(value));
                },
              ),
            ],
          ),
        );
      },
    );
  }
}