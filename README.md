# TODO APP

## Feature
- Add/Update/Delete note

## Data storge
- Local device
## Enhencement
- Authentication
- Cloud data storage
- Notification (Local/Remote)

## Run
```
$ flutter pub run build_runner build --delete-conflicting-outputs
$ flutter run
```
## Testing:
```
$ flutter pub run build_runner build --delete-conflicting-outputs
$ flutter test // run unit of all project
$ flutter test test/$[test_file_path] // run unit of a specific file
```
##  Data & call flow
![image info](https://i0.wp.com/resocoder.com/wp-content/uploads/2019/08/Clean-Architecture-Flutter-Diagram.png?w=556&ssl=1)

[source](https://resocoder.com/2019/08/27/flutter-tdd-clean-architecture-course-1-explanation-project-structure/)

## Code structures

```tree
assets
├── fonts // Asset fonts
└── images // Asset images
lib
├── features // main features of app
│   └── todo_note
│       ├── data // communicate with DB
│       │   ├── repositories // consists classes implemented from `domain/repositories`
│       │   ├── models // models of localDB
│       │   └── datasources // fetch/update data
│       ├── domain // contain the core business logic (use cases) and business objects (entities)
│       │   ├── usecases // contain classes which encapsulate all the business logic of a particular use case of the feature
│       │   ├── entities // business objects (Entity classes)
│       │   └── repositories // contain abstract classes which will implement in `data/repositories`
│       └── presentation // UI and Presentation Logic
│           ├── bloc // BloC directories for each UI page
│           ├── pages // Page directories
│           └── widgets // Widgets used across the pages
├── helper // Helper utility classes
│   └── validator // For the form validation
├── route // Route settings
├── theme // Theme file
└── main.dart // Entry point
test // unit test
    └── features/todo_note
        ├── presentation
        │   │ bloc // unit test bloc
        │   └── ...
        ├── data // 
        │   │ repositories // unit test repository
        │   └── ...
        └── domain // Entry point
            │ usecases // unit test usecases
            └── ...

```
