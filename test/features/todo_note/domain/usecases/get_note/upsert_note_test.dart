import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';
import 'package:todo_app/features/todo_note/domain/usecases/upsert_note.dart';

class MockNoteRepository extends Mock implements NoteRepository {
  @override
  Note upsertNote(Note note) => super.noSuchMethod(
    Invocation.method(#upsertNote, [note]),
    returnValue: Note(isDone: true, title: 'example'),
  );
}

void main() {
  late UpsertNote usecase;
  late MockNoteRepository mockNoteRepository;

  setUp(() {
    mockNoteRepository = MockNoteRepository();
    usecase = UpsertNote(mockNoteRepository);
  });

  final newNote = Note(title: "Note", isDone: true, id: 1);
  final insertedNote = Note(title: "Note", isDone: false, id: 1);

  test(
    'should return upserted notes', 
    () async {
      // Use Mockito to return a inserted note
      when(mockNoteRepository.upsertNote(newNote))
        .thenAnswer((_) => insertedNote);

      // Call method to create a new note
      final result = usecase(newNote);

      // UseCase should simply return inserted note with id
      expect(result, insertedNote);

      // Verify that the method has been called on the Repository
      verify(mockNoteRepository.upsertNote(newNote));

      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockNoteRepository);
    },
  );
}