import 'package:formz/formz.dart';

enum TitleValidationError { empty }

class NoteTitleField extends FormzInput<String?, TitleValidationError> {
  const NoteTitleField.pure([String? value = '']) : super.pure(value);
  const NoteTitleField.dirty([String? value = '']) : super.dirty(value);

  String errorText() {
    switch (error) {
      case TitleValidationError.empty:
        return 'Please enter title';
      
      default:
        return '';
    }
  }

  @override
  TitleValidationError? validator(String? value) {
    return value?.isNotEmpty == true
        ? null
        : TitleValidationError.empty;
  }
}