import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NoContentPage extends StatelessWidget {
  final Color backgroundColor;
  final String text;

  const NoContentPage({
    Key? key,
    this.backgroundColor = Colors.white,
    this.text = 'There isn\'t any content',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width / 2;

    return Material(
      child: Container(
        color: backgroundColor,
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/images/background.svg',
                  width: width,
                ),
                SizedBox(
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.0),
                    child: Text(text,
                      style: Theme.of(context).textTheme.caption!.apply(
                        fontSizeDelta: 2.0,
                        fontWeightDelta: 2,
                      ),
                    )
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
