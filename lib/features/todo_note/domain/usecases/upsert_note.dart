import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';

class UpsertNote {
  final NoteRepository noteRepository;

  UpsertNote(this.noteRepository);

  Note call(Note note) {
    return noteRepository.upsertNote(note);
  }
}