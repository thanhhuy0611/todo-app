import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:todo_app/features/todo_note/data/repositories/note_repository_imp.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/form_note/form_note_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/delete_button.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/description.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/status.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/submit_button.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/title.dart';
import 'package:todo_app/route/route_arguments.dart';

class NoteForm extends StatelessWidget {
  static const pageName = 'form';

  final NoteFormArguments? args;

  const NoteForm({Key? key, this.args}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => FormNoteBloc(
        note: args?.note,
        noteRepository: RepositoryProvider.of<NoteRepositoryImplement>(context),
      ),
      child: BlocListener<FormNoteBloc, FormNoteState>(
        listener: (context, state) {
          if (state.status == FormzStatus.submissionSuccess) {
            Navigator.pop(context, true);
          }
        },
        child: BlocBuilder<FormNoteBloc, FormNoteState>(
          builder: (context, state) {
            // check whether form is update or create 
            bool isUpdate = state.id != null;

            return Scaffold(
              appBar: AppBar(
                title: Text("Form"),
              ),
              body: SafeArea(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: ListView(
                    children: [
                      NoteFormTitle(),
                      NoteFormDescription(),
                      NoteFormStatus(),
                      FormNoteSubmitButton(),

                      isUpdate
                        ? FormNoteDeleteButton()
                        : SizedBox(),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}