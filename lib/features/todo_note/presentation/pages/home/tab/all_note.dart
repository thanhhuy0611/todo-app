import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/home_page/home_page_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/app_loader.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/no_content_page.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/note_item_list/index.dart';
import 'package:collection/collection.dart';

class AllNoteTab extends StatelessWidget {
  const AllNoteTab({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomePageBloc, HomePageState>(
      buildWhen: (prev, curr) => DeepCollectionEquality().equals(prev.allNotes,prev.allNotes),
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            title: Text("All"),
          ),
          body: !state.loaded || state.allNotes == null
            ? AppLoader()
            : state.allNotes!.isEmpty
              ? NoContentPage(text: 'You don\'t have any note. Let\'s add a note!')
              : ListView.builder(
                  itemCount: state.allNotes!.length,
                  itemBuilder: (context, index) {
                    return NoteItemList(note: state.allNotes![index]);
                  },
                ),
        );
      },
    );
  }
}