part of 'form_note_bloc.dart';

abstract class FormNoteEvent {
  const FormNoteEvent();
}

class FormNoteTitleChange extends FormNoteEvent {
  final String title;

  FormNoteTitleChange(this.title);
}

class FormNoteDescriptionChange extends FormNoteEvent {
  final String description;

  FormNoteDescriptionChange(this.description);
}

class FormNoteToggleStatus extends FormNoteEvent {
  final bool isDone;

  FormNoteToggleStatus(this.isDone);
}

class FormNoteSubmit extends FormNoteEvent {}

class FormNoteDelete extends FormNoteEvent {}
