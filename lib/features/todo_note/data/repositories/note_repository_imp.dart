import 'package:todo_app/features/todo_note/data/datasources/note_local_data_source.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/domain/repositories/note_repository.dart';

class NoteRepositoryImplement extends NoteRepository {
  final NoteLocalDataSoure noteLocalDataSoure;

  NoteRepositoryImplement(this.noteLocalDataSoure);

  @override
  Note upsertNote(Note note) {
    return noteLocalDataSoure.upsertNote(note);
  }

  @override
  bool deleteNote(int noteId) {
    return noteLocalDataSoure.deleteNote(noteId);
  }

  @override
  List<Note> getNote({bool? isDone}) {
    return noteLocalDataSoure.getNote(isDone: isDone);
  }
}