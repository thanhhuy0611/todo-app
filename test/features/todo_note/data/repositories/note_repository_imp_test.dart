import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/features/todo_note/data/datasources/note_local_data_source.dart';
import 'package:todo_app/features/todo_note/data/repositories/note_repository_imp.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';

class MockNoteLocalDataSoure extends Mock implements NoteLocalDataSoure {
  @override
  List<Note> getNote({bool? isDone}) => super.noSuchMethod(
    Invocation.method(#getNote, [isDone]),
    returnValue: [Note(isDone: true, title: 'example')].toList()
  );

  @override
  Note upsertNote(Note note) => super.noSuchMethod(
    Invocation.method(#upsertNote, [note]),
    returnValue: Note(isDone: true, title: 'example'),
  );

  @override
  bool deleteNote(int noteId) => super.noSuchMethod(
    Invocation.method(#deleteNote, [noteId]),
    returnValue: true,
  );
}

void main() {
  late MockNoteLocalDataSoure mockNoteLocalDataSoure;
  late NoteRepositoryImplement noteRepositoryImplement;

  setUp(() {
    mockNoteLocalDataSoure = MockNoteLocalDataSoure();
    noteRepositoryImplement = NoteRepositoryImplement(mockNoteLocalDataSoure);
  });

  group('method getNote()', () {
    final mockListNote = List<Note>.from([]);
    test('get list note from NoteLocalDataSource', () async {
      when(mockNoteLocalDataSoure.getNote())
        .thenAnswer((_) => mockListNote);

      final result = noteRepositoryImplement.getNote();
      expect(result, mockListNote);
      // UseCase should simply return all done and todo notes
      expect(result, mockListNote);

      // Verify that the method has been called on the DataSource
      verify(mockNoteLocalDataSoure.getNote());

      // Only the above method should be called and nothing more.
      verifyNoMoreInteractions(mockNoteLocalDataSoure);
    });
  });

  group('method upsertNote()', () {
    final note = Note(title: "Note", isDone: false, id: 1);

    test(
      'should return upserted notes', 
      () async {
        // Use Mockito to return a inserted note
        when(mockNoteLocalDataSoure.upsertNote(note))
          .thenAnswer((_) => note);

        final result = noteRepositoryImplement.upsertNote(note);

        expect(result, note);

        verify(mockNoteLocalDataSoure.upsertNote(note));

        // Only the above method should be called and nothing more.
        verifyNoMoreInteractions(mockNoteLocalDataSoure);
      },
    );
  });

  group('method deleteNote()', () {
    test(
      'should return result true/false', 
      () async {
        final deletedNoteId = 1;

        when(mockNoteLocalDataSoure.deleteNote(deletedNoteId))
          .thenAnswer((_) => true);

        final result = noteRepositoryImplement.deleteNote(deletedNoteId);
        expect(result, true);

        verify(mockNoteLocalDataSoure.deleteNote(deletedNoteId));
        verifyNoMoreInteractions(mockNoteLocalDataSoure);
      },
    );
  });
}