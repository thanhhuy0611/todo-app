import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AppLoader extends StatelessWidget {
  final Color backgroundColor;
  final String text;

  const AppLoader({
    Key? key,
    this.backgroundColor = Colors.white,
    this.text = '',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width / 2;

    return Material(
      child: Container(
        color: backgroundColor,
        child: SafeArea(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/images/background.svg',
                  width: width,
                ),
                SizedBox(
                  width: width,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.0),
                    child: LinearProgressIndicator(color: Theme.of(context).primaryColor),
                  ),
                ),
                text.isNotEmpty
                    ? Text(
                        text,
                        style: const TextStyle(
                          color: Color(0xFF393939),
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                        ),
                      )
                    : const SizedBox()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
