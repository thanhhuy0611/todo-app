
import 'package:todo_app/features/todo_note/domain/entities/note.dart';

class NoteFormArguments {
  final Note? note;

  NoteFormArguments({this.note});
}