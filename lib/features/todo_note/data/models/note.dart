import 'package:objectbox/objectbox.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';

@Entity()
class NoteModel {
  @Id()
  int? id;

  bool isDone;
  String title;
  String? description;
  DateTime date;

  NoteModel({
    this.id,
    required this.title,
    required this.isDone,
    this.description,
    DateTime? date,
  }) : this.date = date ?? DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, DateTime.now().hour, DateTime.now().minute, DateTime.now().second);

  Note toEntity() => Note(
    id: this.id,
    title: this.title,
    description: this.description,
    date: this.date, 
    isDone: this.isDone,
  );

  factory NoteModel.fromEntity(Note note) => NoteModel(
    id: note.id,
    title: note.title,
    description: note.description,
    date: note.date, 
    isDone: note.isDone,
  );

  List<Object?> get props => [id, title, description, isDone];
}