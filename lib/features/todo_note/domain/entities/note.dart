import 'package:equatable/equatable.dart';

class Note extends Equatable {
  final int? id;
  final bool isDone;
  final String title;
  final String? description;
  final DateTime date;

  Note({
    this.id,
    required this.title,
    required this.isDone,
    this.description,
    DateTime? date,
  }) : this.date = date ?? DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day, DateTime.now().hour, DateTime.now().minute, DateTime.now().second);

  Note coppyWith({
    int? id,
    bool? isDone,
    String? title,
    String? description,
    DateTime? date,
  }) {
    return Note(
      id: id ?? this.id,
      isDone: isDone ?? this.isDone,
      title: title ?? this.title,
      date: date ?? this.date,
    );
  }

  List<Object?> get props => [id, title, description, isDone];
}