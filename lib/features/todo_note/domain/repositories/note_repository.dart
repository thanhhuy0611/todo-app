import 'package:todo_app/features/todo_note/domain/entities/note.dart';

abstract class NoteRepository {
  List<Note> getNote({bool? isDone});

  Note upsertNote(Note note);

  bool deleteNote(int noteId);
}