import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/data/repositories/note_repository_imp.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/home_page/home_page_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/pages/home/bottom_bar.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/index.dart';

import 'tab/index.dart';

class HomePage extends StatefulWidget {
  static const pageName = 'home';
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;

  static const List<Widget> _tabViews = [
    AllNoteTab(),
    DoneNoteTab(),
    TodoNoteTab(),
  ];

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => HomePageBloc(
        RepositoryProvider.of<NoteRepositoryImplement>(context),
      )..add(HomePageInit()),
      child: BlocBuilder<HomePageBloc, HomePageState>(
        builder: (context, state) {
          return Scaffold(
            body: IndexedStack(
              index: _currentIndex,
              children: _tabViews,
            ),
            bottomNavigationBar: HomePageBottomBar(
              currentIndex: _currentIndex,
              totalTabs: _tabViews.length,
              onTap: (index) {
                if (_currentIndex != index) {
                  setState(() {
                    _currentIndex = index;
                  });
                }
              },
            ),
            floatingActionButton: FloatingActionButton(
              foregroundColor: Theme.of(context).primaryColor,
              child: Icon(Icons.add, color: Theme.of(context).backgroundColor),
              onPressed: () async {
                var shouldReload = await Navigator.pushNamed(context, NoteForm.pageName);
                if (shouldReload as bool? ?? false) {
                  BlocProvider.of<HomePageBloc>(context).add(HomePageInit());
                }
              }, 
            )
          );
        },
      ),
    );
  }
}