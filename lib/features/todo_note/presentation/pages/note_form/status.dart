import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/form_note/form_note_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/form/input_field_label.dart';

class NoteFormStatus extends StatelessWidget {
  const NoteFormStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FormNoteBloc, FormNoteState>(
      builder: (context, state) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InputFieldLabel('Status'),
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(
                    state.isDone ? 'Done' : 'Todo',
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
                CupertinoSwitch(
                  activeColor: Theme.of(context).primaryColor,
                  value: state.isDone,
                  onChanged: (bool value) {
                    BlocProvider.of<FormNoteBloc>(context).add(
                      FormNoteToggleStatus(value)
                    );
                  },
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}