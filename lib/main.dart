import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/data/datasources/note_local_data_source.dart';
import 'package:todo_app/features/todo_note/data/repositories/note_repository_imp.dart';
import 'package:todo_app/features/todo_note/presentation/pages/home/index.dart';
import 'package:todo_app/route/page_route.dart';
import 'package:todo_app/theme/index.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // Get file directory
  final Directory dir = await getApplicationDocumentsDirectory();
  // init Note local Store
  NoteLocalDataSoure.instance.initialize(dir);

  runApp(TodoApp());
}

class TodoApp extends StatelessWidget {
  const TodoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => NoteRepositoryImplement(NoteLocalDataSoure.instance),
        child: MaterialApp(
          theme: CommonTheme().theme,
          home: HomePage(),
          onGenerateRoute: AppPageRoute.routes,
        ),
    );
  }
}
