import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:formz/formz.dart';
import 'package:mockito/mockito.dart';
import 'package:todo_app/features/todo_note/domain/entities/note.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/form_note/form_note_bloc.dart';
import 'package:todo_app/helper/validator/note/title.dart';

import '../dependency_class.mocks.dart';

void main() {
  final note = Note(title: "Text", isDone: true, id: 1);
  final upsertedNote = Note(title: "Text", isDone: false, id: 1);
  
  late FormNoteBloc bloc;
  late MockNoteRepositoryImplement mockNoteRepository;

  setUp(() {
    mockNoteRepository = MockNoteRepositoryImplement();
    bloc = FormNoteBloc(
      noteRepository: mockNoteRepository,
      note: note,
    );
  });

  tearDown(() {
    bloc.close();
  });

  test('initial state for create form should return empty form', () {
    final createFormState = FormNoteState.init(null);
    
    // inite create FormNoteBloc
    FormNoteBloc creatFormBloc = FormNoteBloc(
      noteRepository: mockNoteRepository,
      note: null,
    );

    expect(creatFormBloc.state, createFormState);
  });

  test('initial state for upsert form should return note detail form', () {
    final upsertFormState = FormNoteState.init(note);
    
    // inite upsert FormNoteBloc
    FormNoteBloc upsertFormBloc = FormNoteBloc(
      noteRepository: mockNoteRepository,
      note: note,
    );

    expect(upsertFormBloc.state, upsertFormState);
  });

  group('FormNoteDelete', () {
    blocTest<FormNoteBloc, FormNoteState>(
      'should call deleteNote from repository 1 time',
      build: () {
        when(mockNoteRepository.deleteNote(any)).thenAnswer((_) => true);
        return bloc;
      },
      act: (bloc) => bloc.add(FormNoteDelete()),
      verify: (_) {
        verify(mockNoteRepository.deleteNote(any)).called(1);
      },
      tearDown: () => bloc.close(),
    );
  });

  group('OnChangedForm', () {
    blocTest<FormNoteBloc, FormNoteState>(
      'should update title of FormNoteState',
      build: () => bloc,
      act: (bloc) => bloc.add(FormNoteTitleChange('Text updated')),
      expect: () => [bloc.state.copyWith(title: NoteTitleField.dirty('Text updated'))],
      tearDown: () => bloc.close(),
    );
  });

  group('FormNoteSubmit', () {
    blocTest<FormNoteBloc, FormNoteState>(
      '(valid): should call upsertNote() and emit FormStatus in order [profress, success, pure]',
      build: () {
        when(mockNoteRepository.upsertNote(any)).thenAnswer((_) => upsertedNote);
        return bloc;
      },
      act: (bloc) => bloc.add(FormNoteSubmit()),
      verify: (_) {
        verify(mockNoteRepository.upsertNote(any)).called(1);
      },
      expect: () => [
        bloc.state.copyWith(status: FormzStatus.submissionInProgress),
        bloc.state.copyWith(status: FormzStatus.submissionSuccess),
        bloc.state.copyWith(status: FormzStatus.pure),
      ],
      tearDown: () => bloc.close(),
    );
  });
}
