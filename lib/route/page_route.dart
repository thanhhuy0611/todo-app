import 'package:flutter/material.dart';
import 'package:todo_app/features/todo_note/presentation/pages/home/index.dart';
import 'package:todo_app/features/todo_note/presentation/pages/note_form/index.dart';
import 'package:todo_app/route/route_arguments.dart';

class AppPageRoute {
  static MaterialPageRoute routes(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) {
        switch (settings.name) {
          case NoteForm.pageName:
            return NoteForm(args: settings.arguments as NoteFormArguments?);
            
          case HomePage.pageName:
          default:
            return HomePage();
        }
      },
    );
  }
}
