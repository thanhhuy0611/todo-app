import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/form_note/form_note_bloc.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/form/input_field_decoration.dart';
import 'package:todo_app/features/todo_note/presentation/widgets/form/input_field_label.dart';

class NoteFormTitle extends StatelessWidget with InputFieldDecoration {
  const NoteFormTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FormNoteBloc, FormNoteState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(vertical: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InputFieldLabel('Title*'),
              TextFormField(
                decoration: inputDecoration(
                  hint: 'What do you want to do?',
                  errText: state.title.invalid
                    ? state.title.errorText()
                    : '',
                ),
                initialValue: state.title.value,
                onChanged: (String value) {
                  BlocProvider.of<FormNoteBloc>(context).add(FormNoteTitleChange(value));
                },
              ),
            ],
          ),
        );
      },
    );
  }
}