part of 'home_page_bloc.dart';

class HomePageState extends Equatable {
  final List<Note>? allNotes;
  final List<Note>? doneNotes;
  final List<Note>? todoNotes;
  final bool loaded;

  const HomePageState({
    this.allNotes, 
    this.doneNotes, 
    this.todoNotes, 
    this.loaded = false,
  });

  HomePageState copyWith({
    List<Note>? allNotes,
    List<Note>? doneNotes,
    List<Note>? todoNotes,
    bool? loaded,
  }) {
    return HomePageState(
      allNotes: allNotes ?? this.allNotes, 
      doneNotes: doneNotes ?? this.doneNotes, 
      todoNotes: todoNotes ?? this.todoNotes, 
      loaded: loaded ?? this.loaded,
    );
  }

  @override
  List<Object?> get props => [
    allNotes, 
    doneNotes, 
    todoNotes, 
    loaded,
  ];
}
