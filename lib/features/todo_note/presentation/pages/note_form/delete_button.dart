import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:formz/formz.dart';
import 'package:todo_app/features/todo_note/presentation/bloc/form_note/form_note_bloc.dart';

class FormNoteDeleteButton extends StatelessWidget {
  const FormNoteDeleteButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<FormNoteBloc, FormNoteState>(
      builder: (context, state) {
        return Container(
          margin: EdgeInsets.symmetric(vertical: 16),
          height: 48,
          width: double.infinity,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: Theme.of(context).errorColor,
              shape: StadiumBorder(),
            ),
            child: Text('Delete',
              style: Theme.of(context).textTheme.button!.apply(
                fontWeightDelta: 2,
                fontSizeDelta: 2,
              ),
            ),
            onPressed: state.status == FormzStatus.submissionInProgress
              ? null
              : () => BlocProvider.of<FormNoteBloc>(context).add(FormNoteDelete()), 
          ),
        );
      },
    );
  }
}