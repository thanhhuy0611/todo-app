part of 'home_page_bloc.dart';

abstract class HomePageEvent {
  const HomePageEvent();
}

class HomePageInit extends HomePageEvent {}

class HomePageToggleNote extends HomePageEvent {
  final Note note;
  final bool status;

  HomePageToggleNote({required this.note, required this.status});
}