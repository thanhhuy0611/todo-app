import 'package:formz/formz.dart';

enum DescriptionValidationError { empty }

class NoteDescriptionField extends FormzInput<String?, DescriptionValidationError> {
  const NoteDescriptionField.pure([String? value = '']) : super.pure(value);
  const NoteDescriptionField.dirty([String? value = '']) : super.dirty(value);

  @override
  DescriptionValidationError? validator(String? value) {
    return null;
  }
}